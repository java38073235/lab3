package dawson;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */

    @Test
    public void shouldReturn5()
    {
        App application=new App();
        int a=application.echo(5);
        assertEquals(5,a);
    }

    @Test
    public void shouldReturn1More()
    {
        App application=new App();
        int a=application.oneMore(5);
        assertEquals(6,a);
    }
}
